oem-scripts (0.99) focal; urgency=medium

  [ Yuan-Chen Cheng]
  * launchpad-api: refine message and add handy test.
  * umadision, debian/links, setup.py, tests/test_flake8: add script to
    query qa.debian for ubuntu pkg because it response faster.

  [ Jeremy Szu ]
  * recovery-from-iso.sh: with stella image support.
    Need to use the image build after pc-stella-cmit-focal-amd64-X00-20210617-1562.iso
    because the ubuntu recovery needs to support recovery_type=dev.
    This patch is just for supporting re-deploy, the auto sanity check
    will be supported in the future.
    Inject ssh key, nopasswd for automation

 -- Yuan-Chen Cheng <yc.cheng@canonical.com>  Wed, 07 Jul 2021 11:37:50 +0800

oem-scripts (0.98) focal; urgency=medium

  * Black all Python files.
  * debian/control,
    debian/rules: Use black to check all Python files.
  * tests/test_flake8: Ignore W503 because it is incompatible with PEP 8.
  * tests/test_flake8: Ignore E203 because flake8 doesn't deal with it well
    and black will cover it.
  * debian/rules,
    tests/test_black: Move the black check script out of debian/rules so
    people can use it to test Python files directly.
  * debian/control,
    debian/rules,
    tests/test_flake8,
    tests/test_pep8: Remove the pep8 check because flake8 will use
    pycodestyle (formerly called pep8) to check.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 11 Jun 2021 06:37:37 +0000

oem-scripts (0.97) focal; urgency=medium

  * pkg-oem-meta: Add linux-oem-20.04c parameter support

 -- Kai-Chuan Hsieh <kaichuan.hsieh@canonical.com>  Mon, 07 Jun 2021 15:12:48 +0800

oem-scripts (0.96) focal; urgency=medium

  * debian/tests/setup-apt-dir,
    setup-apt-dir.sh: Add an option to enable i386 arch.
  * debian/tests/pkg-list,
    pkg-list: Make it able to deal with i386 dependencies and remove the
    deprecated verbose option.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 31 May 2021 09:10:07 +0000

oem-scripts (0.95) focal; urgency=medium

  * setup-apt-dir.sh: Add a new option to support /var/lib/dpkg/status.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 21 May 2021 17:32:51 +0800

oem-scripts (0.94) focal; urgency=medium

  * debian/tests/mir-bug-verification,
    debian/tests/pkg-list,
    oem-meta-packages,
    pkg-list: Rename the option to avoid ambiguous meaning.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 17 May 2021 06:24:55 +0000

oem-scripts (0.93) focal; urgency=medium

  * debian/tests/lp-dl-attm,
    lp-dl-attm,
    setup.py: Add a new tool to download all attachments of the bug.
  * debian/tests/control,
    debian/tests/mir-bug: Skip the test of mir-bug because it is buggy.
  * debian/tests/lp-dl-attm,
    lp-dl-attm: Support the private bug.
  * lp-dl-attm: Refine the usage and check if a valid bug id. 
  * debian/rules,
    tests/test_shellcheck: Use shellcheck to check lp-dl-attm.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 14 May 2021 05:37:50 +0000

oem-scripts (0.92) focal; urgency=medium

  * debian/tests/launchpad-api,
    launchpad-api: Support PATCH method.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 13 May 2021 01:50:25 +0000

oem-scripts (0.91) focal; urgency=medium

  * pkg-list: Fix the problem when checking the dependencies.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 10 May 2021 10:18:23 +0000

oem-scripts (0.90) focal; urgency=medium

  * oem-meta-packages: Collect the bootstrap meta version from Git repository.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 06 May 2021 12:15:03 +0000

oem-scripts (0.89) focal; urgency=medium

  * setup-apt-dir.sh: Add '--disable-base' option to disable the base archive.
  * oem-meta-packages: Collect the versions in Ubuntu archive and OEM archive.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 06 May 2021 04:09:09 +0000

oem-scripts (0.88) focal; urgency=medium

  * oem-meta-packages: Avoid the already subscription problem.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 30 Apr 2021 01:56:13 +0000

oem-scripts (0.87) focal; urgency=medium

  * debian/tests/oem-meta-packages,
    oem-meta-packages: Add --meta option to specify the meta package to
    collect the information.
  * debian/tests/mir-bug: Adjust the mir-bug test against the oldest one to
    avoid the missing Git repo problem.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 29 Apr 2021 02:41:42 +0000

oem-scripts (0.86) focal; urgency=medium

  * oem-meta-packages: Don't check the factory meta in the apt cache.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 26 Apr 2021 11:47:11 +0800

oem-scripts (0.85) focal; urgency=medium

  * oem-meta-packages: Add a question before dput and after git push.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 21 Apr 2021 16:37:40 +0800

oem-scripts (0.84) focal; urgency=medium

  * bin-bug: add "-m,--main" to bind common bugs to main bugs in a private
    project.

 -- Alex Tu <alex.tu@canonical.com>  Tue, 20 Apr 2021 15:08:14 +0800

oem-scripts (0.83) focal; urgency=medium

  * oem-meta-packages,
    pkg-oem-meta: Avoid the installation issue in Ubuntu live system.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 19 Apr 2021 13:12:39 +0800

oem-scripts (0.82) focal; urgency=medium

  * debian/tests/control,
    debian/tests/review-merge-proposal,
    setup.py: Add the test and fix the installation for review-merge-proposal.
  * debian/tests/pkg-stella-meta,
    pkg-oem-meta: Make the modalias pattern to match the subsystem ID of the
    i2c device.
  * debian/tests/control,
    debian/tests/recovery-from-iso.sh,
    setup.py: Add the test and fix the installation for recovery-from-iso.sh.
  * mir-bug: Fix the SSID parsing issue for the i2c device.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 12 Apr 2021 15:37:40 +0000

oem-scripts (0.81) focal; urgency=medium

  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Make the modalias pattern to match the subsystem ID of the
    i2c device.
  * mir-bug: Fix the version string.
  * mir-bug: Print the data link of the attachment.
  * review-merge-proposal,
    tests/test_flake8,
    tests/test_pep8: Create review-merge-proposal to check the merge proposal.
  * debian/tests/oem-meta-packages,
    oem-meta-packages: Add an option to the 'update' subcommand to also
    add/update the kernel meta into the depends of the factory meta.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 30 Mar 2021 08:30:07 +0000

oem-scripts (0.80) focal; urgency=medium

  [ Shih-Yuan Lee (FourDollars) ]
  * debian/control,
    debian/tests/control,
    debian/tests/lp-bug,
    lp-bug: Add the 'cqa-verify' subcommand to verify those bugs with the
    'cqa-verified-staging' tag.
  * oem-meta-packages: Refine the help manual.
  * lp-bug: Update the manual of 'cqa-verify' subcommand, put more comments in
    the source code, and refine some code.
  * lp-bug: Fix the list copy.

 -- root <root@5022153d-7586-4af9-445e-f53d89194342>  Tue, 02 Feb 2021 10:57:04 +0000

oem-scripts (0.79) focal; urgency=medium

  * oem-meta-packages: Fix the problem when it falls back to linux-oem-20.04.
  * oem-meta-packages: Ignore the warnings for the maintainer scripts.
  * oem-meta-packages: Fix the modified detection problem of debian/postrm.
  * oem-meta-packages: Skip the staging-lock bugs with cqa-verified-staging. 

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 01 Feb 2021 03:26:08 +0000

oem-scripts (0.78) focal; urgency=medium

  * oem-meta-packages,
    pkg-oem-meta: Improve to deal with the maintainer scripts and linux-oem-20.04b.
  * debian/tests/oem-meta-packages,
    oem-meta-packages: Refine for the usage of a single meta package.
  * oem-meta-packages: Deal with the case when no version in staging archive.
  * oem-meta-packages: Use warning instead of critical when the version in Git
    is not published to PPA.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 28 Jan 2021 08:17:33 +0000

oem-scripts (0.77) focal; urgency=medium

  * oem-meta-packages: Add 'staging-copy' subcommand.
  * debian/tests/setup-apt-dir,
    setup-apt-dir.sh: Add --disable-updates and --disable-backports to disable
    updates and backports channels.
  * config.sh,
    debian/tests/setup-apt-dir: Use login anonymously to test.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 19 Jan 2021 09:48:57 +0000

oem-scripts (0.76) focal; urgency=medium

  * oem-meta-packages: Add 'collect' subcommand to generate metapackages info.
  * oem-meta-packages: Refactor 'update' subcommand to process metapackages info.
  * setup-apt-dir.sh: Add '--apt-dir', '--extra-repo' and '--extra-key' options.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 15 Jan 2021 03:55:09 +0000

oem-scripts (0.75) focal; urgency=medium

  * oem-meta-packages,
    pkg-oem-meta: Refine debian/postinst in OEM metapackages.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 11 Jan 2021 08:15:56 +0000

oem-scripts (0.74) focal; urgency=medium

  * debian/control,
    oem-meta-packages: Add 'update' subcommand to change the kernel flavour,
    kernel dependency and grub flavour.
  * debian/tests/control,
    debian/tests/oem-meta-packages: Add the autopkgtest for oem-meta-packages.
  * setup-apt-dir.sh: Use the longer fingerprint to avoid the key import
    issue.
  * oem_scripts/__init__.py: Bump to 0.74.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 06 Jan 2021 14:53:19 +0800

oem-scripts (0.73) focal; urgency=medium

  * mir-bug: Fix the usage of pkg-oem-meta.
  * mir-bug: Deal with new exception.
  * debian/tests/mir-bug: Check the latest MIR bug to avoid the test failure.
  * oem_scripts/__init__.py: Bump to 0.73.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 30 Dec 2020 14:22:58 +0800

oem-scripts (0.72) focal; urgency=medium

  * debian/tests/pkg-somerville-meta,
    debian/tests/pkg-stella-meta,
    debian/tests/pkg-sutton-meta,
    pkg-oem-meta: Support linux-oem-20.04b.
  * oem_scripts/__init__.py: Bump to 0.72.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 29 Dec 2020 15:32:14 +0800

oem-scripts (0.71) focal; urgency=medium

  * pkg-list: Add an option '--fail-unavailable' to return unavailable error.
  * pkg-list: Refine the checking for depends.
  * oem-meta-packages,
    oem_scripts/LaunchpadLogin.py,
    setup.py,
    tests/test_flake8,
    tests/test_pep8: Add oem-meta-packages to subscribe the bugs of oem meta
    packages.
  * oem_scripts/__init__.py: Bump to 0.71.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 04 Dec 2020 14:50:37 +0800

oem-scripts (0.70) focal; urgency=medium

  * mir-bug,
    oem_scripts/logging.py,
    pkg-oem-meta: Print warning messages in pkg-oem-meta when DEBFULLNAME or
    DEBEMAIL is invalid, and append the public bug into debian/changelog by
    mir-bug when the public bug is unavailable.
  * autopkgtest-oem-scripts-auto: Update the PPA for oem-scripts.
  * debian/tests/mir-bug: Polish the test.
  * run-autopkgtest: Add the license header and add the target option.
  * debian/tests/control,
    debian/tests/mir-bug-verification,
    mir-bug: Add a test to check all verification-needed MIR bugs.
  * oem_scripts/LaunchpadLogin.py: Support login anonymously when there is no
    token.
  * debian/tests/mir-bug-verification: Polish the test for mir bugs verification.
  * mir-bug: Make the JSON output compact and sorted by keys.
  * debian/tests/jq-lp,
    debian/tests/launchpad-api,
    debian/tests/mir-bug-verification,
    get-private-ppa,
    launchpad-api,
    oem_scripts/LaunchpadLogin.py: Refine anonymously login and support
    LAUNCHPAD_TOKEN.
  * oem_scripts/__init__.py: Bump to 0.70.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 24 Nov 2020 17:07:39 +0800

oem-scripts (0.69) focal; urgency=medium

  * debian/tests/control
    debian/tests/setup-apt-dir
    setup-apt-dir.sh: Make setup-apt-dir.sh to set up apt dir.
  * debian/tests/pkg-list,
    pkg-list: Make pkg-list support specified apt dir.
  * debian/tests/mir-bug: Skip checking the real meta.
  * oem_scripts/__init__.py: Bump to 0.69.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 23 Nov 2020 10:32:26 +0800

oem-scripts (0.68) focal; urgency=medium

  * debian/tests/control,
    debian/tests/mir-bug,
    debian/tests/pkg-list,
    pkg-list,
    setup.py,
    tests/test_flake8,
    tests/test_pep8: Make pkg-list to generate the package list.
  * oem_scripts/__init__.py: Bump to 0.68.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 18 Nov 2020 23:54:31 +0800

oem-scripts (0.67) focal; urgency=medium

  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Fix oem-flavour.cfg installing path.
  * oem_scripts/__init__.py: Bump to 0.67.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 11 Nov 2020 14:54:38 +0800

oem-scripts (0.66) focal; urgency=medium

  * debian/control,
    debian/copyright: Move Vcs to the public.
  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Set GRUB_FLAVOUR_ORDER in /etc/default/grub.d/oem-flavour.cfg.
  * debian/tests/mir-bug,
    mir-bug: Add an option to collect ubuntu-certified MIR bugs.
  * oem_scripts/__init__.py: Bump to 0.66.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 04 Nov 2020 16:48:40 +0800

oem-scripts (0.65) focal; urgency=medium

  * debian/tests/get-private-ppa,
    debian/tests/launchpad-api,
    get-private-ppa,
    launchpad-api: Fix the Launchpad staging API issue.
  * .gitignore,
    .shellspec,
    spec/oem-scripts_spec.sh,
    spec/spec_helper.sh: Introduce https://shellspec.info/.
  * debian/tests/get-private-ppa: Refine the test script.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 27 Oct 2020 14:58:21 +0800

oem-scripts (0.64) focal; urgency=medium

  [ Yuan-Chen Cheng ]
  * bug-bind.py: correctly detect if target has been associated.

  [ Shih-Yuan Lee (FourDollars) ]
  * bug-bind.py: Use oem_scripts.LaunchpadLogin for Launchpad API.
  * debian/tests/bug-bind,
    debian/tests/control: Add the autopkgtest for bug-bind.py.
  * debian/tests/mir-bug,
    lp-bug,
    mir-bug,
    setup.py,
    tests/test_flake8,
    tests/test_pep8: Move the generic 'remove' subcommand to lp-bug, and add
    'copy' subcommand to lp-bug.
  * debian/tests/bug-bind,
    lp-bug: Improve the autopkgtest for bug-bind by copying a private bug.
  * lp-bug: Fix the problem when the bug is not public.
  * debian/tests/bug-bind: Any output from stderr will break the test so it is
    redirected to stdout.
  * debian/tests/bug-bind,
    debian/tests/mir-bug,
    lp-bug: Rename the 'remove' subcommand to 'cleanup'.
  * lp-bug: Fix the wrong web_link.
  * debian/tests/bug-bind,
    lp-bug: Add an option '--public' for the 'copy' subcommand to avoid the
    "HTTP Error 503" issue on Launchpad staging when cleaning up the bug.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 23 Oct 2020 14:30:00 +0800

oem-scripts (0.63) focal; urgency=medium

  * oem_scripts/__init__.py: Bump to 0.63.
  * mir-bug: When updating the bug, delete the bug_task of Ubuntu if it is not
    ready, and add the example for 'remove' subcommand.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 21 Oct 2020 17:08:48 +0800

oem-scripts (0.62) focal; urgency=medium

  * oem_scripts/__init__.py: Bump to 0.62.
  * debian/tests/mir-bug,
    mir-bug: Check and update bug status, check and update Git repository,
    remove old oem-scripts-* tag when updating tags, remove the create test,
    hardcode TZ=UTC-8 for oem-metapackage-mir-check for consistent debdiff,
    correct the usage of attachments and subscriptions for Launchpad API, and
    update the attachments of debdiff, add '--tz' options for 'check' and
    'update' subcommands, check and update subscriptions, polish the help
    manual, add 'remove' subcommand to remove all information from the bug,
    fix the modifying issues of title and description, and remove the '(WIP)'
    wording of the 'update' subcommand.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 19 Oct 2020 12:49:53 +0800

oem-scripts (0.61) focal; urgency=medium

  * debian/control,
    debian/tests/mir-bug,
    oem_scripts/__init__.py,
    mir-bug: Refactor mir-bug to create subcommand, add other subcommands and
    don't also affect 'Ubuntu' when creating MIR bug.
  * debian/control,
    debian/rules,
    tests/test_flake8: Test mir-bug by flake8.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 08 Oct 2020 18:07:28 +0800

oem-scripts (0.60) focal; urgency=medium

  * run-autopkgtest: Try to support different architecture.
  * run-autopkgtest: Fix the typo of mirror option.
  * debian/control: Recommend zsync-curl instead of depending on it.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 08 Oct 2020 10:20:39 +0800

oem-scripts (0.59) focal; urgency=medium

  * run-autopkgtest: Fix some typos.
  * autopkgtest-collect-credentials,
    run-autopkgtest: Add a new option '--copy' of autopkgtest-collect-credentials
    and use it in run-autopkgtest to copy files into the testbed.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 29 Sep 2020 09:56:17 +0800

oem-scripts (0.58) focal; urgency=medium

  [ Jeremy Su ]
  * debian/tests/control,
    debian/tests/pkg-stella-meta: Add stella meta pkgs test
  * pkg-oem-meta: Depends group meta in stella.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 28 Sep 2020 21:40:09 +0800

oem-scripts (0.57) focal; urgency=medium

  * run-autopkgtest: Use credentials when running tests instead of including
    credentials in the testbed, enrich the help manual, and start to support
    vm-ubuntu-cloud builder.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 25 Sep 2020 12:28:28 +0800

oem-scripts (0.56) focal; urgency=medium

  [ Shih-Yuan Lee (FourDollars) ]
  * debian/control,
    debian/tests/control,
    debian/tests/run-autopkgtest,
    run-autopkgtest,
    setup.py: Add an autopkgtest wrapper to make life easier.

  [ Bin Li ]
  * mir-bug: Remove pilot running followed the change in lp:1889367.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 24 Sep 2020 23:56:21 +0800

oem-scripts (0.55) focal; urgency=medium

  [ Shih-Yuan Lee (FourDollars) ]
  * oem_scripts/LaunchpadLogin.py,
    oem_scripts/__init__.py,
    setup.py: Add the oem_scripts.LaunchpadLogin python module.
  * debian/control,
    debian/tests/control,
    debian/tests/mir-bug,
    mir-bug,
    setup.py,
    tests/test_pep8: Add mir-bug, pep8 test and its autopkgtest.

  [ Bin Li ]
  * Add mir-bug script to report a mir bug.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 10 Sep 2020 20:13:46 +0800

oem-scripts (0.54.1) focal; urgency=medium

  * debian/rules: removed python units test
  * setup.py: add python unit tests

 -- Rex Tsai (蔡志展) <rex.tsai@canonical.com>  Tue, 01 Sep 2020 22:22:15 +0800

oem-scripts (0.54) focal; urgency=medium

  * debian/gitlab-ci.yml: Add gitlab-ci.yml for focal.
  * bug-bind.py, tests/test_bugbind.py, tests/test_pep8, setup.py: Add bug-bind (from lp-fish-tools)
  * debian/control: add python3-launchpadlib into build-depends.
  * debian/rules: override dh_auto_test

 -- Rex Tsai (蔡志展) <rex.tsai@canonical.com>  Tue, 01 Sep 2020 13:18:23 +0800

oem-scripts (0.53) focal; urgency=medium

  * debian/tests/get-private-ppa,
    get-private-ppa: Add options to specify the codename, generate the
    source list content, and print help manual.
  * debian/tests/launchpad-api,
    launchpad-api: Reserve LAUNCHPAD_URL and LAUNCHPAD_API for Launchpad
    staging.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 14 Aug 2020 11:42:03 +0800

oem-scripts (0.52) focal; urgency=medium

  * debian/tests/control,
    debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Fix the problem when gbp import-dsc.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 27 Jul 2020 15:48:05 +0800

oem-scripts (0.51) focal; urgency=medium

  * debian/tests/pkg-somerville-meta,
    debian/tests/pkg-sutton-meta,
    pkg-oem-meta: Fix the filename of test case for easier comparison with
    oem-qemu-meta.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 27 Jul 2020 12:05:25 +0800

oem-scripts (0.50) focal; urgency=medium

  [ Cyrus Lien ]
  * autopkgtest-collect-credentials,
    debian/tests/control,
    debian/tests/git-url-insteadof-setting: Make git support lp: prefix.

  [ Shih-Yuan Lee (FourDollars) ]
  * debian/tests/pkg-somerville-meta,
    debian/tests/pkg-sutton-meta,
    pkg-oem-meta: Keep UNRELEASED in the bootstrap meta package.
  * autopkgtest-oem-scripts-auto,
    debian/tests/control: Fix the autopkgtest testbed setup script for
    git-url-insteadof-setting.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 20 Jul 2020 17:37:01 +0800

oem-scripts (0.49) focal; urgency=medium

  * get-private-ppa,
    launchpad-api: Check the status code by httpie and use it to check the subscription
    of private PPA.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 17 Jul 2020 14:50:58 +0800

oem-scripts (0.48) focal; urgency=medium

  * pkg-oem-meta: Bump debhelper from 11 to 12.
  * pkg-oem-meta: Set debhelper-compat version in Build-Depends.
  * pkg-oem-meta: Update standards version to 4.5.0, no changes needed.
  * pkg-oem-meta: Adjust the description for factory packages.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 16 Jul 2020 13:48:03 +0800

oem-scripts (0.47) focal; urgency=medium

  * copyPackage.py: Fix some PEP8 errors.
  * pkg-oem-meta: Use the public Git repository.
  * pkg-oem-meta: Remove the first empty line in gbp.conf.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 22 Jun 2020 17:47:34 +0800

oem-scripts (0.46) focal; urgency=medium

  [ Shih-Yuan Lee (FourDollars) ]
  * debian/tests/control,
    debian/tests/jq-pipe,
    jq-pipe,
    setup.py: Add a script to make pipe line eaiser.
  * autopkgtest-collect-credentials,
    autopkgtest-oem-scripts-auto: Use bash strict mode.
  * jq-pipe: Refine the bash strict mode.
  * jq-lp: Rename from jq-pipe.
  * debian/tests/control,
    debian/tests/jq-lp: Fix the test case for jq-lp.
  * setup.py: Fix the installing for jq-lp.

  [ Bin Li]
  * jq-lp: Get the path of jq-lp, it's same path with launchpad-api.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 02 Jun 2020 13:12:44 +0800

oem-scripts (0.45) bionic; urgency=medium

  * launchpad-api: Fix some problems by shellcheck. 
  * debian/tests/launchpad-api,
    launchpad-api: Make the api url more flexible.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 01 Jun 2020 14:10:20 +0800

oem-scripts (0.44) bionic; urgency=medium

  [ Bin Li]
  * pkg-oem-meta: Add the dependency for group meta and group facotry meta for
    sutton.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 25 May 2020 20:42:11 +0800

oem-scripts (0.43) bionic; urgency=medium

  * autopkgtest-collect-credentials,
    autopkgtest-oem-scripts-auto: Fix the permission of SSH private key.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 20 May 2020 15:05:32 +0800

oem-scripts (0.42) bionic; urgency=medium

  * autopkgtest-collect-credentials,
    autopkgtest-oem-scripts-auto,
    debian/tests/autopkgtest-collect-credentials,
    debian/tests/control,
    setup.py: Implement the general purposed autopkgtest-collect-credentials.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 20 May 2020 12:48:32 +0800

oem-scripts (0.41) bionic; urgency=medium

  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Make Somerville factory meta depend on
    oem-somerville-factory-meta.
  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Add debian/gbp.conf for the meta packages.
  * pkg-oem-meta: Generate the test case for autopkgtest.
  * debian/control: Remove XS-Testsuite field.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 19 May 2020 16:59:07 +0800

oem-scripts (0.40) bionic; urgency=medium

  [ Jeremy Su ]
  * debian/tests/pkg-sutton-meta,
    pkg-oem-meta: Correct the Git branch names for Stella and Sutton.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 18 May 2020 13:05:11 +0800

oem-scripts (0.39) bionic; urgency=medium

  [ Bin Li]
  * autopkgtest-oem-scripts-auto
    get-private-ppa,
    launchpad-api: Add the usage in scripts and use absolute path to make them
    work without the installation.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 15 May 2020 15:58:17 +0800

oem-scripts (0.38) bionic; urgency=medium

  * autopkgtest-oem-scripts-auto: Make it able to be used by autopkgtest-build-qemu.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 14 May 2020 22:02:22 +0800

oem-scripts (0.37) bionic; urgency=medium

  [ Bin Li]
  * pkg-oem-meta: Fix the information in parameter

  [Shih-Yuan Lee (FourDollars)]
  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Fix the wrong kernel option and add more tests for it.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 12 May 2020 17:59:23 +0800

oem-scripts (0.36) bionic; urgency=medium

  * autopkgtest-oem-scripts-auto,
    debian/tests/autopkgtest-oem-scripts-auto,
    debian/tests/control: Use shellcheck to check the output.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Sat, 09 May 2020 00:18:01 +0800

oem-scripts (0.35) bionic; urgency=medium

  * autopkgtest-oem-scripts-auto,
    config.sh: Add the section name.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 08 May 2020 22:03:01 +0800

oem-scripts (0.34) bionic; urgency=medium

  * config.sh,
    launchpad-api: Make it work System-wide.
  * autopkgtest-oem-scripts-auto,
    config.sh,
    get-private-ppa,
    launchpad-api: Avoid the system-wide token transition problem.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 08 May 2020 20:56:11 +0800

oem-scripts (0.33) bionic; urgency=medium

  * get-private-ppa: Make it also support the public PPA.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 08 May 2020 16:54:53 +0800

oem-scripts (0.32) bionic; urgency=medium

  * autopkgtest-oem-scripts-auto,
    debian/tests/autopkgtest-oem-scripts-auto,
    debian/tests/control: Add a autopkgtest case.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 08 May 2020 14:54:29 +0800

oem-scripts (0.31) bionic; urgency=medium

  * autopkgtest-oem-scripts-auto: Only support a POSIX shell script.
  * autopkgtest-oem-scripts-auto,
    debian/tests/get-private-ppa,
    get-private-ppa: Simulate the source list made by add-apt-repository.
  * autopkgtest-oem-scripts-auto: Make it easier to use multiple PPA.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 08 May 2020 13:09:26 +0800

oem-scripts (0.30) bionic; urgency=medium

  * get-private-ppa: Support multiple PPA.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 07 May 2020 23:57:54 +0800

oem-scripts (0.29) bionic; urgency=medium

  [ Bin Li]
  * pkg-oem-meta: Fix the case sensitive problem for pkg-sutton-meta.
  * debian/tests/control,
    debian/tests/pkg-sutton-meta: Add the autopkgtest case for pkg-sutton-meta.

  [Shih-Yuan Lee (FourDollars)]
  * debian/tests/pkg-somerville-meta,
    debian/tests/pkg-sutton-meta,
    pkg-oem-meta: Use group name and archive, and add common source list.
  * debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Depend on oem-somerville-meta for somerville real meta.
  * pkg-oem-meta: The dummy should not depend on any kernel meta.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 07 May 2020 19:22:38 +0800

oem-scripts (0.28) bionic; urgency=medium

  * launchpad-api: Ignore stdin to avoid failing in autopkgtest.
  * debian/control: Break and replace lp-fish-tools (<<1.192.40)

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 07 May 2020 12:24:08 +0800

oem-scripts (0.27) bionic; urgency=medium

  * autopkgtest-oem-scripts-auto,
    config.sh,
    get-private-ppa,
    launchpad-api.sh: Change the function names in config.sh.
  * autopkgtest-oem-scripts-auto,
    debian/control,
    debian/tests/control,
    debian/tests/get-private-ppa,
    debian/tests/launchpad-api,
    get-private-ppa,
    launchpad-api.sh,
    setup.py: Rename launchpad-api.sh to launchpad-api and add some test cases.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 07 May 2020 00:29:54 +0800

oem-scripts (0.26) bionic; urgency=medium

  [Shih-Yuan Lee (FourDollars)]
  * debian/control,
    debian/copyright: Add the Git repository.
  * debian/oem-scripts.install: Rename to debian/install.
  * debian/install,
    usr/bin: Move all scripts under bin.
  * debian/control: The priority extra has been deprecated.
  * debian/control,
    debian/tests/control: Add some example for autopkgtest.
  * debian/control,
    debian/install,
    setup.py: Migrate to Python3.
  * bin/*: Move all scripts under the top folder of the package.
  * pkg-oem-meta: Copy from pkg-somerville-meta in lp-fish-tools 1.192.39.
  * pkg-oem-meta: Adjust for other OEM projects.
  * pkg-oem-meta: Support stella and sutton.
  * pkg-oem-meta: Capitalize the OEM and platform name and reduce some
    Exceptions.
  * pkg-oem-meta: Add public and private bug options for all OEM projects.
  * debian/rules,
    get-oem-auth-token,
    get-oemshare-auth-token,
    oem-getiso,
    tests/test_pep8: Clear some PEP8 errors.
  * get-oemshare-auth-token: Make it work on Python3.
  * get-oem-auth-token: Make it work on Python3.
  * oem-getiso: Make it work on Python3.
  * debian/control: Build depends on pep8.
  * debian/links: Add links for pkg-somerville-meta, pkg-stella-meta and
    pkg-sutton-meta.
  * debian/control: Depend on python3-distro-info.
  * debian/control,
    debian/tests/control,
    debian/tests/pkg-somerville-meta,
    pkg-oem-meta: Add the test case for autopkgtest.
  * tests/test_pep8: Check pkg-oem-meta.
  * auto-oem-scripts-autopkgtest,
    config.sh,
    get-private-ppa,
    launchpad-api.sh,
    setup.py: Add some scripts to prepare the setup script for autopkgtest.
  * auto-oem-scripts-autopkgtest,
    setup.py: Rename to autopkgtest-oem-scripts-auto.

  [ Bin Li]
  * pkg-oem-meta: Fix warning for Exception and strict rules for bios/sd information.
  * pkg-oem-meta: Support the bvn for sutton.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Wed, 06 May 2020 21:49:34 +0800

oem-scripts (0.25) trusty; urgency=medium

  * dkms-helper:
    - Add dkms-helper information in debian/control.
    - Force using BUILD_EXCLUSIVE_KERNEL to limit the kernel major and minor
    numbers.
    - Support KO_REGEX and BUILD_EXCLUSIVE_KERNEL for dkms-helper.sh
    - Add Vcs-Bzr support.
    - Add modalias manually adding support.
    - Add firmware package support for dkms-helper.
    - Show help manual when there is no argument.
    - Avoid the installation issue of DKMS caused by Debian packaging.
    - Need to export KVER variable.
    - Add -d|--distribution -m|--message to dkms-helper.sh
    - Add setup() to dkms-helper.sh

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 30 Dec 2016 20:40:19 +0800

oem-scripts (0.24) trusty; urgency=medium

  * oem-getiso
    - Allow using iso and iso-hybrid image type
      in URL. (LP: #1560831)

 -- Franz Hsieh (Franz) <franz.hsieh@canonical.com>  Fri, 08 Apr 2016 16:21:38 +0800

oem-scripts (0.23) trusty; urgency=medium

  * oem-getiso
    - Fix the download URL for iso-hybrid. (LP: #1560831)

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 31 Mar 2016 16:45:08 +0800

oem-scripts (0.22) precise; urgency=medium

  * get-oem-auth-token:
    - Add retry mechanism
    - Add RetryLimitExceed exception
    - Catch user interrupt exception
    - Catch unknown exceptions 
    - Return exit code

 -- Franz Hsieh (Franz) <franz.hsieh@canonical.com>  Fri, 01 Aug 2014 12:42:51 +0800

oem-scripts (0.21) precise; urgency=low

  * oem-getiso
    - Fix missing / that cause wrong iso url
    - Return zsync/rsync exit code instead of 0

 -- Franz Hsieh (Franz) <franz.hsieh@canonical.com>  Tue, 29 Oct 2013 16:22:11 +0800

oem-scripts (0.20) precise; urgency=low

  * dkms-helper: Fix the issue of Makefile.orig that causes installation
    failed.
  * Switch to dpkg-source 3.0 (native) format

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 28 Oct 2013 16:51:53 +0800

oem-scripts (0.19) precise; urgency=low

  * dkms-helper: Fix the firmware support when the firmware directory is not exit.

 -- Shawn Wang <shawn.wang@canonical.com>  Thu, 25 Jul 2013 16:43:39 +0800

oem-scripts (0.18) precise; urgency=low

  * dkms-helper: Fix the firmware support when dealing with tarball.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Thu, 25 Jul 2013 13:18:57 +0800

oem-scripts (0.17) precise; urgency=low

  * Improve dkms-helper.
    - Add firmware support

 -- Shawn Wang <shawn.wang@canonical.com>  Tue, 09 Jul 2013 17:25:34 +0800

oem-scripts (0.16) precise; urgency=low

  * Improve dkms-helper.
    - Add ${HOME}/.dkms-helper.env support.
    - Add more customized options, such as AUTOINSTALL, MODULES_CONF and
    REMAKE_INITRD.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Tue, 11 Jun 2013 18:12:38 +0800

oem-scripts (0.15) precise; urgency=low

  * Add dkms-helper.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Fri, 07 Jun 2013 18:02:01 +0800

oem-scripts (0.14) precise; urgency=low

  * usr/bin/oem-getiso
    - remove old image and .zs-old file after zsync image
    - sync image even the file is exist. (left for zsync and rsync
      to handle)

 -- Franz Hsieh (Franz) <franz.hsieh@canonical.com>  Tue, 07 May 2013 14:54:38 +0800

oem-scripts (0.13) precise; urgency=low

  * usr/bin/get-oem-auth-token
    - Copied from get-oemshare-auth-token.
    - add both oem-share and oem-ibs authentication support.
    - add --username --password --code to support silent mode.
  * usr/bin/oem-getiso
    - Changed the call() method to return exit code

 -- Franz Hsieh (Franz) <franz.hsieh@canonical.com>  Mon, 06 May 2013 17:49:42 +0800

oem-scripts (0.12) precise; urgency=low

  * usr/bin/live-build-image-chroot.sh
   - Now support to genreate a new squashfs image.


 -- Rex Tsai (蔡志展) <rex.tsai@canonical.com>  Tue, 30 Apr 2013 16:38:02 +0800

oem-scripts (0.11) precise; urgency=low

  * usr/bin/live-build-image-chroot.sh
    - Add live-build-image-chroot.sh to chroot into the squashfs filesystem of
    images made by live-build.

 -- Shih-Yuan Lee (FourDollars) <sylee@canonical.com>  Mon, 22 Apr 2013 15:33:42 +0800

oem-scripts (0.10) precise; urgency=low

  * usr/bin/oem-getiso
    - Changed the call() method for zsync downloading to use shell=True so
      that it can open the seed, if detected, and correctly calculate the
      delta.

 -- Timothy Chavez <timothy.chavez@canonical.com>  Thu, 28 Feb 2013 19:51:06 -0600

oem-scripts (0.9) precise; urgency=low

  [ Timothy Chavez ]
  * added get-oemshare-auth-token to get am authentication token for oem-share
    that can be used for command line scripts that need retrieve images

  * added support for zsync-curl to oem-getiso so that images can be retrieved
    with zsync over https.

  [ Hsin-Yi Chen (hychen) ]
  
  * debian/control: 
    - added zsync-curl, python-mechanize into deb depends.
    - bump standard version.
  * oem-getiso: removed duplicat cods.

 -- Hsin-Yi Chen (hychen) <hychen@canonical.com>  Mon, 18 Feb 2013 16:46:36 +0800

oem-scripts (0.8) precise; urgency=low

  * added an ability to name remote Laptop to setup4test.sh script (--rn|--remotenm)
    it is used in synergy

 -- Alex Wolfson <alex.wolfson@canonical.com>  Tue, 14 Aug 2012 16:20:08 -0400

oem-scripts (0.7) oneiric; urgency=low

  * added oem-getiso script: download iso from oem-share by rsync

 -- Hsin-Yi Chen (hychen) <hychen@canonical.com>  Mon, 26 Dec 2011 17:15:28 +0800

oem-scripts (0.6) oneiric; urgency=low

  * added .bzr-builddeb
  * modified the algorithm to find not in bzr files. The old way had a bug
    when a subdirectory was a part of not in bzr subdirectory

 -- Alex Wolfson <alex.wolfson@canonical.com>  Thu, 08 Dec 2011 10:15:29 -0500

oem-scripts (0.5) natty; urgency=low

  * rename-everything: Fixed a bug that prevented correct bzr update,
    when directory processed under source control
  * added config file name as a parameter, it was hardcoded before

 -- Alex Wolfson <alex.wolfson@canonical.com>  Wed, 05 Oct 2011 09:03:23 -0400

oem-scripts (0.4) natty; urgency=low

  * Corrected the rev number

 -- Alex Wolfson <alex.wolfson@canonical.com>  Fri, 09 Sep 2011 13:52:40 -0400

oem-scripts (0.3ubuntu7) natty; urgency=low

  * changed setup4test.sh to reflect renaming of systemtap-... to stap-...

 -- Alex Wolfson <alex.wolfson@canonical.com>  Mon, 29 Aug 2011 14:28:58 -0400

oem-scripts (0.3ubuntu6) natty; urgency=low

  * renamed system tap scripts again from systemtap-... to stap-...

 -- Alex Wolfson <alex.wolfson@canonical.com>  Mon, 29 Aug 2011 14:25:04 -0400

oem-scripts (0.3ubuntu5) natty; urgency=low

  * systemtap and setup4test.sh: renamed systemtap scripts to systemtap-dbgsym.sh
    and systemtap-build-mymodule.sh

 -- Alex Wolfson <alex.wolfson@canonical.com>  Mon, 29 Aug 2011 14:18:29 -0400

oem-scripts (0.3ubuntu4) natty; urgency=low

  * setup-systemtap-mymodule.sh : added -h --help options

 -- Alex Wolfson <alex.wolfson@canonical.com>  Sat, 27 Aug 2011 22:50:21 -0400

oem-scripts (0.3ubuntu3) natty; urgency=low

  * setup4test added systemtap related scripts to bzr

 -- Alex Wolfson <alex.wolfson@canonical.com>  Sat, 27 Aug 2011 21:51:43 -0400

oem-scripts (0.3ubuntu2) natty; urgency=low

  * added copying system tap related scripts to remote /home/$USER/systemtap

 -- Alex Wolfson <alex.wolfson@canonical.com>  Fri, 26 Aug 2011 16:38:15 -0400

oem-scripts (0.3ubuntu1) natty; urgency=low

  * setup4test added -a option to usrmod command, when setting up systemtap

 -- Alex Wolfson <alex.wolfson@canonical.com>  Tue, 23 Aug 2011 13:44:57 -0400

oem-scripts (0.3) natty; urgency=low

  * in setup4test.sh changed the location of pctests to ~/pctests

 -- Alex Wolfson <alex.wolfson@canonical.com>  Wed, 17 Aug 2011 15:46:59 -0400

oem-scripts (0.2) natty; urgency=low

  * changed script names to use '-' instead of '_'
  * added the option of installing system tap to setup4test.sh

 -- Alex Wolfson <alex.wolfson@canonical.com>  Tue, 16 Aug 2011 17:00:56 -0400

oem-scripts (0.1) natty; urgency=low

  * Initial release

 -- Alex Wolfson <alex.wolfson@canonical.com>  Mon, 15 Aug 2011 15:20:40 -0400
